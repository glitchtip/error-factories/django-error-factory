from django.urls import path

from .views import (
    CustomPerformanceView,
    DatabaseErrorView,
    DatabaseStackErrorView,
    DivideZeroView,
    FingerprintView,
    HomePageView,
    LoggerErrorView,
    LoggerWarningView,
    MessageView,
    PostErrorView,
    TagView,
    TemplateErrorView,
)

urlpatterns = [
    path("", HomePageView.as_view(), name="home"),
    path("divide-zero/", DivideZeroView.as_view(), name="divide_zero"),
    path("database-error/", DatabaseErrorView.as_view(), name="database_error"),
    path("post-error/", PostErrorView.as_view(), name="post_error"),
    path(
        "database-stack-error/",
        DatabaseStackErrorView.as_view(),
        name="database_stack_error",
    ),
    path("template-error/", TemplateErrorView.as_view(), name="template_error"),
    path("message/", MessageView.as_view(), name="message"),
    path("logger-warning/", LoggerWarningView.as_view(), name="logger-warning"),
    path("logger-error/", LoggerErrorView.as_view(), name="logger-error"),
    path("tag-error/", TagView.as_view(), name="tag-error"),
    path(
        "custom-performance/",
        CustomPerformanceView.as_view(),
        name="custom-performance",
    ),
    path("fingerprint/", FingerprintView.as_view(), name="fingerprint")
]
