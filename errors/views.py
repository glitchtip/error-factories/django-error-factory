import logging
from random import randrange

from django.contrib.auth.models import User
from django.http import HttpResponse
from django.views import View
from django.views.generic.base import TemplateView
from sentry_sdk import (capture_exception, capture_message, push_scope,
                        set_context, set_tag, start_span, start_transaction)

logger = logging.getLogger(__name__)


class HomePageView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        set_tag("good_tag", "foo")
        set_tag("bad_tag", True)
        return super().get_context_data(**kwargs)


class CustomPerformanceView(View):
    def get(self, request, *args, **kwargs):
        with start_transaction(op="task", name="foo_name"):
            with start_span(op="subtask", description="make a bad span tag") as span:
                span.set_tag("bad_span", True)
        return HttpResponse("OK")


class DivideZeroView(View):
    def get(self, request, *args, **kwargs):
        0 / 0


class DatabaseErrorView(View):
    def get(self, request, *args, **kwargs):
        User.objects.get(id="9999999")


class PostErrorView(View):
    def post(self, request, *args, **kwargs):
        request.POST["nope"]


class DatabaseStackErrorView(View):
    def get(self, request, *args, **kwargs):
        users = User.objects.all()
        self.make_error(users)

    def make_error(self, users):
        User.objects.get(id=users.count() + 10000)


class TemplateErrorView(TemplateView):
    template_name = "template_error.html"


class MessageView(View):
    def get(self, request, *args, **kwargs):
        capture_message("Page not found!")
        return HttpResponse("Ok")


class LoggerWarningView(View):
    def get(self, request, *args, **kwargs):
        set_tag("good_tag", "foo")
        set_context("some_context", {"foo": "bar"})
        logger.warning("This is a warning: %s", "warning")
        return HttpResponse("Ok")


class LoggerErrorView(View):
    def get(self, request, *args, **kwargs):
        try:
            0 / 0
        except Exception as e:
            logger.error("This is an error", exc_info=e)
        return HttpResponse("Ok")


class TagView(View):
    def get(self, request, *args, **kwargs):
        set_tag("test_tag", "the value")
        0 / 0

class FingerprintView(View):
    def get(self, request, *args, **kwargs):
        try:
            raise Exception("something %s" % randrange(10))
        except Exception as err:
            with push_scope() as scope:
                scope.fingerprint = ["foo"]
                capture_exception(err)
        return HttpResponse("Ok")
