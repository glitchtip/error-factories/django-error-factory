# How to run

Ensure Docker and Docker Compose are installed

`docker-compose up`

# How to make errors

Set the Sentry DSN to your Sentry or Glitchtip project's DSN

- `cp docker-compose.yml docker-compose.override.yml`
- Edit the override file and set the `SENTRY_DSN`
- docker-compose up
- Navigate to the error you'd like to generate

To inspect the SDK payload, enable tracing in GlitchTip
