FROM python:3.13
ENV PYTHONUNBUFFERED=1 \
  PORT=8080 \
  UV_COMPILE_BYTECODE=1 \
  UV_SYSTEM_PYTHON=true \
  UV_PYTHON_DOWNLOADS=never \
  UV_PROJECT_ENVIRONMENT=/usr/local

RUN mkdir /code
WORKDIR /code
COPY --from=ghcr.io/astral-sh/uv:latest /uv /bin/uv
COPY pyproject.toml uv.lock /code/
RUN uv sync --frozen --no-install-project

COPY . /code/

